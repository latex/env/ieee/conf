# Paper preparation

## Sources:

- I. Stojmenovic, "[Editor's Note: How to Write Research Articles in Computing and Engineering Disciplines,](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=5372121&isnumber=5372118)" in IEEE Transactions on Parallel and Distributed Systems, vol. 21, no. 2, pp. 145-147, Feb. 2010, doi: 10.1109/TPDS.2010.12. 
- Stojmenović, Ivan, and Veljko Milutinović. "[How to write research articles in computing and engineering disciplines.](https://scindeks.ceon.rs/article.aspx?artid=2217-80901201042S)" Singidunum journal of applied sciences 9.1 (2012): 42-50.

## Compilation

The compilation can be done with `../Makefile` with the command: 
```bash
make preparation
```