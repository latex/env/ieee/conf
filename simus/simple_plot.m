% a simple script to illustrate how to make a plot to be included in a paper

load census.mat

plot(cdate,pop,'s-','LineWidth',2)
xlim([min(cdate) max(cdate)])
xlabel('Year')
ylabel('Population (millions)')
title('US population data from 1790 - 1990.')
grid on

filename_pdf = 'simple_plot.pdf';
saveas(gcf,filename_pdf)

% Set LD_LIBRARY_PATH to fix the error : "libtiff.so.5: version `LIBTIFF_4.0' not found"
% related issues (but not giving the solution used here)
%  - https://askubuntu.com/questions/943009/gs-matlab-bin-glnxa64-libtiff-so-5-no-version-information-available-requir
%  - https://fr.mathworks.com/matlabcentral/answers/1936489-libtiff-so-5-version-libtiff_4-0-not-found-required-by-so-for-matlab-in-ubuntu-environment?s_tid=srchtitle
system(['LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/ pdfcrop --verbose ' filename_pdf ' ' filename_pdf])

