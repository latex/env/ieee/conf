# Conference Template

An environment for IEEE conference papers. 

## Initial Setup

Source documents : found on [Manuscript Templates for Conference Proceedings]([https://](https://www.ieee.org/conferences/publishing/templates.html))

[LaTeX Template Instructions (PDF, 63 KB)](http://www.ctan.org/tex-archive/macros/latex/contrib/IEEEtran/IEEEtran_HOWTO.pdf) [Be sure to use the template's conference mode.]

 - [Template (ZIP, 700 KB)](https://www.ieee.org/content/dam/ieee-org/ieee/web/org/pubs/conference-latex-template_10-17-19.zip) Updated October 2019
 - [LaTeX Bibliography Files (ZIP, 309 KB)](https://www.ieee.org/content/dam/ieee-org/ieee/web/org/conferences/IEEEtranBST2.zip)

Accessed on 2023/04/26
